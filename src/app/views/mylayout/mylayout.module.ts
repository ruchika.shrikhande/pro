import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomepageComponent } from './homepage/homepage.component';
import { VegComponent } from './veg/veg.component';
import { NvegComponent } from './nveg/nveg.component';
import { TraditionalComponent } from './traditional/traditional.component';
import { DrinksComponent } from './drinks/drinks.component';



@NgModule({
  declarations: [DashboardComponent, HomepageComponent, VegComponent, NvegComponent, TraditionalComponent, DrinksComponent],
  imports: [
    CommonModule
  ]
})
export class MylayoutModule { }
