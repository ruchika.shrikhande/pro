import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NvegComponent } from './nveg.component';

describe('NvegComponent', () => {
  let component: NvegComponent;
  let fixture: ComponentFixture<NvegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NvegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NvegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
